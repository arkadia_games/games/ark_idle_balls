#!/usr/bin/env python3

from pathlib import Path
import sys;
import os;

filenames = [];
decls     = [];

root_dir    = os.path.abspath(".");
output_file = os.path.join(root_dir, "resources/textures/textures_to_load.g.js");

for path in Path(root_dir).glob("**/*.png"):
    clean_path = str(path).replace(root_dir, "")[1:];
    quoted_path = "\"{0}\"".format(clean_path)
    filenames.append(quoted_path);

    varname = str(clean_path)  \
        .replace("./",     "") \
        .replace(root_dir, "") \
        .replace(".png",   "") \
        .replace("/",     "_") \
        .upper();

    decl = "const {varname} = {filename}".format(
        varname  = varname,
        filename = quoted_path
    );
    decls.append(decl);

with open(output_file, "w") as f:
    f.write("\n".join(decls));
    f.write("\n\n");
    f.write("const TEXTURES_TO_LOAD = [ {0} ]".format(
        ",\n".join(filenames)
    ))
