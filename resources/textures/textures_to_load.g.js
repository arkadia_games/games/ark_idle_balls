const RESOURCES_FONTS_BIG_FONT = "resources/fonts/big_font.png"
const RESOURCES_FONTS_MEDIUM_FONT = "resources/fonts/medium_font.png"
const RESOURCES_FONTS_SMALL_FONT = "resources/fonts/small_font.png"
const RESOURCES_TEXTURES_LEADERBOARDS = "resources/textures/Leaderboards.png"
const RESOURCES_TEXTURES_LOGO = "resources/textures/LOGO.png"
const RESOURCES_TEXTURES_TEST = "resources/textures/test.png"
const RESOURCES_TEXTURES_BACKGROUND_CLOUD_0 = "resources/textures/background/cloud_0.png"
const RESOURCES_TEXTURES_BACKGROUND_CLOUD_1 = "resources/textures/background/cloud_1.png"
const RESOURCES_TEXTURES_BACKGROUND_CLOUD_2 = "resources/textures/background/cloud_2.png"
const RESOURCES_TEXTURES_BRICKS_1 = "resources/textures/bricks/1.png"
const RESOURCES_TEXTURES_BRICKS_2 = "resources/textures/bricks/2.png"
const RESOURCES_TEXTURES_BRICKS_3 = "resources/textures/bricks/3.png"
const RESOURCES_TEXTURES_BRICKS_4 = "resources/textures/bricks/4.png"
const RESOURCES_TEXTURES_BRICKS_5 = "resources/textures/bricks/5.png"
const RESOURCES_TEXTURES_BUTTONS_BOX_BLUE = "resources/textures/buttons/box/blue.png"
const RESOURCES_TEXTURES_BUTTONS_BOX_BLUE_PRESSED = "resources/textures/buttons/box/blue_pressed.png"
const RESOURCES_TEXTURES_BUTTONS_BOX_GREEN = "resources/textures/buttons/box/green.png"
const RESOURCES_TEXTURES_BUTTONS_BOX_GREEN_PRESSED = "resources/textures/buttons/box/green_pressed.png"
const RESOURCES_TEXTURES_BUTTONS_BOX_RED = "resources/textures/buttons/box/red.png"
const RESOURCES_TEXTURES_BUTTONS_BOX_RED_PRESSED = "resources/textures/buttons/box/red_pressed.png"
const RESOURCES_TEXTURES_BUTTONS_BOX_YELLOW = "resources/textures/buttons/box/yellow.png"
const RESOURCES_TEXTURES_BUTTONS_BOX_YELLOW_PRESSED = "resources/textures/buttons/box/yellow_pressed.png"
const RESOURCES_TEXTURES_BUTTONS_ICON_ARROW_LEFT = "resources/textures/buttons/icon/arrow_left.png"
const RESOURCES_TEXTURES_BUTTONS_ICON_CREDITS = "resources/textures/buttons/icon/credits.png"
const RESOURCES_TEXTURES_BUTTONS_ICON_LEADERBOARDS = "resources/textures/buttons/icon/leaderboards.png"
const RESOURCES_TEXTURES_BUTTONS_ICON_MUSIC_OFF = "resources/textures/buttons/icon/music_off.png"
const RESOURCES_TEXTURES_BUTTONS_ICON_MUSIC_ON = "resources/textures/buttons/icon/music_on.png"
const RESOURCES_TEXTURES_BUTTONS_ICON_PLAY = "resources/textures/buttons/icon/play.png"
const RESOURCES_TEXTURES_BUTTONS_ICON_PLUS = "resources/textures/buttons/icon/plus.png"
const RESOURCES_TEXTURES_FADE_SHAPE_5 = "resources/textures/fade/Shape_5.png"
const RESOURCES_TEXTURES_PARTICLES_EMOTE_HEART = "resources/textures/particles/emote_heart.png"
const RESOURCES_TEXTURES_PARTICLES_EMOTE_HEARTS = "resources/textures/particles/emote_hearts.png"
const RESOURCES_TEXTURES_PARTICLES_EMOTE_STAR = "resources/textures/particles/emote_star.png"
const RESOURCES_TEXTURES_PARTICLES_EMOTE_STARS = "resources/textures/particles/emote_stars.png"
const RESOURCES_TEXTURES_SCENARIO_BUSH1 = "resources/textures/scenario/bush1.png"
const RESOURCES_TEXTURES_SCENARIO_BUSH2 = "resources/textures/scenario/bush2.png"
const RESOURCES_TEXTURES_SCENARIO_BUSH3 = "resources/textures/scenario/bush3.png"
const RESOURCES_TEXTURES_SCENARIO_BUSH4 = "resources/textures/scenario/bush4.png"
const RESOURCES_TEXTURES_SCENARIO_BUSHALT1 = "resources/textures/scenario/bushAlt1.png"
const RESOURCES_TEXTURES_SCENARIO_BUSHALT2 = "resources/textures/scenario/bushAlt2.png"
const RESOURCES_TEXTURES_SCENARIO_BUSHALT3 = "resources/textures/scenario/bushAlt3.png"
const RESOURCES_TEXTURES_SCENARIO_BUSHALT4 = "resources/textures/scenario/bushAlt4.png"
const RESOURCES_TEXTURES_SCENARIO_BUSHORANGE1 = "resources/textures/scenario/bushOrange1.png"
const RESOURCES_TEXTURES_SCENARIO_BUSHORANGE2 = "resources/textures/scenario/bushOrange2.png"
const RESOURCES_TEXTURES_SCENARIO_BUSHORANGE3 = "resources/textures/scenario/bushOrange3.png"
const RESOURCES_TEXTURES_SCENARIO_BUSHORANGE4 = "resources/textures/scenario/bushOrange4.png"
const RESOURCES_TEXTURES_SCENARIO_FENCE = "resources/textures/scenario/fence.png"
const RESOURCES_TEXTURES_SCENARIO_HOUSE1 = "resources/textures/scenario/house1.png"
const RESOURCES_TEXTURES_SCENARIO_HOUSE2 = "resources/textures/scenario/house2.png"
const RESOURCES_TEXTURES_SCENARIO_HOUSEALT1 = "resources/textures/scenario/houseAlt1.png"
const RESOURCES_TEXTURES_SCENARIO_HOUSEALT2 = "resources/textures/scenario/houseAlt2.png"
const RESOURCES_TEXTURES_SCENARIO_HOUSESMALL1 = "resources/textures/scenario/houseSmall1.png"
const RESOURCES_TEXTURES_SCENARIO_HOUSESMALL2 = "resources/textures/scenario/houseSmall2.png"
const RESOURCES_TEXTURES_SCENARIO_HOUSESMALLALT1 = "resources/textures/scenario/houseSmallAlt1.png"
const RESOURCES_TEXTURES_SCENARIO_HOUSESMALLALT2 = "resources/textures/scenario/houseSmallAlt2.png"
const RESOURCES_TEXTURES_SCENARIO_SLICE02_02 = "resources/textures/scenario/slice02_02.png"
const RESOURCES_TEXTURES_SCENARIO_SLICE03_03 = "resources/textures/scenario/slice03_03.png"
const RESOURCES_TEXTURES_SCENARIO_SLICE04_04 = "resources/textures/scenario/slice04_04.png"
const RESOURCES_TEXTURES_SCENARIO_SLICE33_33 = "resources/textures/scenario/slice33_33.png"
const RESOURCES_TEXTURES_SCENARIO_TREE = "resources/textures/scenario/tree.png"
const RESOURCES_TEXTURES_SCENARIO_TREEDEAD = "resources/textures/scenario/treeDead.png"
const RESOURCES_TEXTURES_SCENARIO_TREELONG = "resources/textures/scenario/treeLong.png"
const RESOURCES_TEXTURES_SCENARIO_TREELONGORANGE = "resources/textures/scenario/treeLongOrange.png"
const RESOURCES_TEXTURES_SCENARIO_TREELONGSNOW = "resources/textures/scenario/treeLongSnow.png"
const RESOURCES_TEXTURES_SCENARIO_TREEORANGE = "resources/textures/scenario/treeOrange.png"
const RESOURCES_TEXTURES_SCENARIO_TREEPALM = "resources/textures/scenario/treePalm.png"
const RESOURCES_TEXTURES_SCENARIO_TREEPINE = "resources/textures/scenario/treePine.png"
const RESOURCES_TEXTURES_SCENARIO_TREEPINEORANGE = "resources/textures/scenario/treePineOrange.png"
const RESOURCES_TEXTURES_SCENARIO_TREEPINESNOW = "resources/textures/scenario/treePineSnow.png"
const RESOURCES_TEXTURES_SCENARIO_TREESMALL_GREEN1 = "resources/textures/scenario/treeSmall_green1.png"
const RESOURCES_TEXTURES_SCENARIO_TREESMALL_GREEN2 = "resources/textures/scenario/treeSmall_green2.png"
const RESOURCES_TEXTURES_SCENARIO_TREESMALL_GREEN3 = "resources/textures/scenario/treeSmall_green3.png"
const RESOURCES_TEXTURES_SCENARIO_TREESMALL_GREENALT1 = "resources/textures/scenario/treeSmall_greenAlt1.png"
const RESOURCES_TEXTURES_SCENARIO_TREESMALL_GREENALT2 = "resources/textures/scenario/treeSmall_greenAlt2.png"
const RESOURCES_TEXTURES_SCENARIO_TREESMALL_GREENALT3 = "resources/textures/scenario/treeSmall_greenAlt3.png"
const RESOURCES_TEXTURES_SCENARIO_TREESMALL_ORANGE1 = "resources/textures/scenario/treeSmall_orange1.png"
const RESOURCES_TEXTURES_SCENARIO_TREESMALL_ORANGE2 = "resources/textures/scenario/treeSmall_orange2.png"
const RESOURCES_TEXTURES_SCENARIO_TREESMALL_ORANGE3 = "resources/textures/scenario/treeSmall_orange3.png"
const RESOURCES_TEXTURES_SCENARIO_TREESNOW = "resources/textures/scenario/treeSnow.png"

const TEXTURES_TO_LOAD = [ "resources/fonts/big_font.png",
"resources/fonts/medium_font.png",
"resources/fonts/small_font.png",
"resources/textures/Leaderboards.png",
"resources/textures/LOGO.png",
"resources/textures/test.png",
"resources/textures/background/cloud_0.png",
"resources/textures/background/cloud_1.png",
"resources/textures/background/cloud_2.png",
"resources/textures/bricks/1.png",
"resources/textures/bricks/2.png",
"resources/textures/bricks/3.png",
"resources/textures/bricks/4.png",
"resources/textures/bricks/5.png",
"resources/textures/buttons/box/blue.png",
"resources/textures/buttons/box/blue_pressed.png",
"resources/textures/buttons/box/green.png",
"resources/textures/buttons/box/green_pressed.png",
"resources/textures/buttons/box/red.png",
"resources/textures/buttons/box/red_pressed.png",
"resources/textures/buttons/box/yellow.png",
"resources/textures/buttons/box/yellow_pressed.png",
"resources/textures/buttons/icon/arrow_left.png",
"resources/textures/buttons/icon/credits.png",
"resources/textures/buttons/icon/leaderboards.png",
"resources/textures/buttons/icon/music_off.png",
"resources/textures/buttons/icon/music_on.png",
"resources/textures/buttons/icon/play.png",
"resources/textures/buttons/icon/plus.png",
"resources/textures/fade/Shape_5.png",
"resources/textures/particles/emote_heart.png",
"resources/textures/particles/emote_hearts.png",
"resources/textures/particles/emote_star.png",
"resources/textures/particles/emote_stars.png",
"resources/textures/scenario/bush1.png",
"resources/textures/scenario/bush2.png",
"resources/textures/scenario/bush3.png",
"resources/textures/scenario/bush4.png",
"resources/textures/scenario/bushAlt1.png",
"resources/textures/scenario/bushAlt2.png",
"resources/textures/scenario/bushAlt3.png",
"resources/textures/scenario/bushAlt4.png",
"resources/textures/scenario/bushOrange1.png",
"resources/textures/scenario/bushOrange2.png",
"resources/textures/scenario/bushOrange3.png",
"resources/textures/scenario/bushOrange4.png",
"resources/textures/scenario/fence.png",
"resources/textures/scenario/house1.png",
"resources/textures/scenario/house2.png",
"resources/textures/scenario/houseAlt1.png",
"resources/textures/scenario/houseAlt2.png",
"resources/textures/scenario/houseSmall1.png",
"resources/textures/scenario/houseSmall2.png",
"resources/textures/scenario/houseSmallAlt1.png",
"resources/textures/scenario/houseSmallAlt2.png",
"resources/textures/scenario/slice02_02.png",
"resources/textures/scenario/slice03_03.png",
"resources/textures/scenario/slice04_04.png",
"resources/textures/scenario/slice33_33.png",
"resources/textures/scenario/tree.png",
"resources/textures/scenario/treeDead.png",
"resources/textures/scenario/treeLong.png",
"resources/textures/scenario/treeLongOrange.png",
"resources/textures/scenario/treeLongSnow.png",
"resources/textures/scenario/treeOrange.png",
"resources/textures/scenario/treePalm.png",
"resources/textures/scenario/treePine.png",
"resources/textures/scenario/treePineOrange.png",
"resources/textures/scenario/treePineSnow.png",
"resources/textures/scenario/treeSmall_green1.png",
"resources/textures/scenario/treeSmall_green2.png",
"resources/textures/scenario/treeSmall_green3.png",
"resources/textures/scenario/treeSmall_greenAlt1.png",
"resources/textures/scenario/treeSmall_greenAlt2.png",
"resources/textures/scenario/treeSmall_greenAlt3.png",
"resources/textures/scenario/treeSmall_orange1.png",
"resources/textures/scenario/treeSmall_orange2.png",
"resources/textures/scenario/treeSmall_orange3.png",
"resources/textures/scenario/treeSnow.png" ]