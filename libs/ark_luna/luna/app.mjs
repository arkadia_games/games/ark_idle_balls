import { Scene_Manager } from "./scene_manager.mjs";
import { Layout        } from "./layout.mjs";
import { Tween_Group   } from "./tween.mjs";

export class App
{
    //
    // Private Vars
    //
    //--------------------------------------------------------------------------
    static _callbacks      = null;
    static _config         = null;
    static _pixi_instance  = null;
    static _max_delta_time = 0;

    //
    // Public Vars
    //
    //--------------------------------------------------------------------------
    static delta_time = 0;
    static total_time = 0;
    // Callbacks
    static pre_init = null;
    static pre_load = null;
    static init     = null;
    static loop     = null;
    static resize   = null;

    //
    // Configuration
    //
    //--------------------------------------------------------------------------
    static set_config(config)
    {
        App._config = config;
        // @XXX(stdmatt): Make this react to window resize - 7/27/2021, 5:54:40 AM
        Layout.DESIGN_WIDTH  = App._config.design_size.x;
        Layout.DESIGN_HEIGHT = App._config.design_size.y;
    }

    //
    // Public Functions
    //
    //--------------------------------------------------------------------------
    static async start()
    {
        //
        // Init before PIXI...
        if(App.pre_init) App.pre_init();

        //
        // Init Pixi itself.
        App._setup_pixi();

        //
        // Init with PIXI.
        if(App.pre_load) await App.pre_load();
        if(App.init)           App.init    ();

        //
        // Setup update loop.
        App._pixi_instance.ticker.add(
            delta => {
                // Cap the delta time.
                let dt = App._pixi_instance.ticker.deltaMS / 1000;
                if(dt > App._max_delta_time) {
                    dt = App._max_delta_time;
                }
                // Update timers.
                App.delta_time  = dt;
                App.total_time += dt;

                // Call the game loop.
                if(App.loop) App.loop(dt);

                // Update the tweens.
                const tween_groups = Tween_Group.get_tagged_groups();
                for(let [_, group] of tween_groups) {
                    group.update(dt);
                }

                // Update the scene.
                Scene_Manager.update(dt);
            }
        );
    }

    //
    // Helpers
    //
    //--------------------------------------------------------------------------
    static set_clear_color(color)
    {
        App._pixi_instance.renderer.backgroundColor = color;
    }


    //
    // PIXI
    //
    //--------------------------------------------------------------------------
    static get_pixi_instance()
    {
        return App._pixi_instance;
    }

    //--------------------------------------------------------------------------
    static _setup_pixi()
    {
        // Create PIXI app.
        App._pixi_instance = new PIXI.Application({
            width  : App._config.design_size.x,
            height : App._config.design_size.y
        });
        document.body.appendChild(App._pixi_instance.view);

        // Set the timers.
        App._max_delta_time = 1.0 / App._config.target_fps;

        // Create the PIXI particles.
        if((typeof _MCOW_HACK_PIXI_PARTICLES_INIT) == "function") {
            _MCOW_HACK_PIXI_PARTICLES_INIT(PIXI);
        }
    }

    //
    // Other
    //
    static display_hello()
    {
        console.log("Hello from luna!");
    }
};
