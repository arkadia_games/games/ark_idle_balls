//
//
//
export class GUI
{
    static _g = null;

    static init()
    {
        GUI._g = new dat.GUI();
        _G = GUI._g
    }

    static make(obj)
    {
        const root = _G.addFolder(obj.constructor.name);
        root.open();
        GUI._make(obj, root, 0) ;
    }

    static _make(obj, folder, i) {
        if( i > 10) {
            return;
        }

        for (let key in obj) {
            const value = obj[key];

            if(value == null) {
                // _G.add(obj, key);
                continue;
            }

            const type_of_value = typeof(value);
            if(type_of_value == "object" ) {
                const sub_folder = folder.addFolder(key);
                sub_folder.open();
                GUI._make(value, sub_folder, i + 1);
                continue;
            }

            if(type_of_value == "function") {
                continue;
            }

            folder.add(obj, key);
        }
    }
}





//
//
//
let _G = null;
