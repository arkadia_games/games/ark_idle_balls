import { RES } from "./res.mjs";


// @XXX(stdmatt): This is a first implementation so not caring too much
// of how the api is being developed, but there's a lot of complications
// arrising by the fact that we don't have the methods with enough
// clarity points.
// I mean, would be better to have a clear object tree and the functions
// operate upon the objects of that object tree - 8/2/2021, 3:34:43 AM
export class Fixed_Size_Container
    extends PIXI.Container
{
    //--------------------------------------------------------------------------
    constructor(width, height, color = 0xFF00FF)
    {
        super();

        this.bg = RES.create_sprite_with_white_texture(width, height);
        this.bg.alpha = 0.5;
        this.bg.tint  = color;

        this.addChild(this.bg);
    }

    //--------------------------------------------------------------------------
    _calculateBounds()
    {
    } // _calculateBounds
}
