import { luna } from "../../libs/ark_luna/luna/luna.mjs"
import { Parallax_Layer } from "./parallax_layer.mjs";

//
// Constants
//
const SKY_BACKGROUND_PARALLAX_SPEED          = 50;
const SKY_BACKGROUND_GROUND_HEIGHT           = 120;
const SKY_BACKGROUND_LAYER_OFFSET_MULTIPLIER = 0.7;
const SKY_BACKGROUND_BACKGROUND_COLOR        = 0xCFEFFC;

const SKY_BACKGROUND_TEXTURES = [
    RESOURCES_TEXTURES_BACKGROUND_CLOUD_2,
    RESOURCES_TEXTURES_BACKGROUND_CLOUD_0,
    RESOURCES_TEXTURES_BACKGROUND_CLOUD_1,
];


//
// Sky Background
//
export class Sky_Background
    extends PIXI.Container
{
    //--------------------------------------------------------------------------
    constructor()
    {
        super();

        // Background color
        const bg = luna.RES.create_sprite_with_white_texture(luna.Layout.DESIGN_WIDTH, luna.Layout.DESIGN_HEIGHT);
        bg.tint = SKY_BACKGROUND_BACKGROUND_COLOR;
        this.addChild(bg);

        // Background Parallax
        this.background_layers = [];
        this._create_parallax_layers();
    } // CTOR

    //--------------------------------------------------------------------------
    update(dt)
    {
        const speed = -(SKY_BACKGROUND_PARALLAX_SPEED * dt);
        for(let i = 0; i < this.background_layers.length; ++i) {
            const layer  = this.background_layers[i];
            const factor = 1 - (i / this.background_layers.length);
            layer.move_parallax(speed, factor);
        }
    } // Update

    //--------------------------------------------------------------------------
    _create_parallax_layers()
    {
        for(let i = 0; i < SKY_BACKGROUND_TEXTURES.length; ++i) {
            const texture_name = SKY_BACKGROUND_TEXTURES[i];
            const texture      = luna.RES.get_texture(texture_name);
            const layer        = new Parallax_Layer(texture, luna.Layout.DESIGN_WIDTH);

            const prev_layer = (i == 0) ? null : this.background_layers[i-1];
            if(prev_layer) {
                layer.y = prev_layer.y + (prev_layer.height * SKY_BACKGROUND_LAYER_OFFSET_MULTIPLIER) - layer.height;
            } else {
                layer.y = luna.Layout.DESIGN_HEIGHT - layer.height - SKY_BACKGROUND_GROUND_HEIGHT;
            }
            this.background_layers.push(layer);
        }

        // Add inverted...
        for(let i = this.background_layers.length -1; i >= 0; --i) {
            this.addChild(this.background_layers[i]);
        }
    } // _CreateParallaxLayers
} // class SkyBackground
