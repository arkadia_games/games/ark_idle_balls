import { luna } from "../../libs/ark_luna/luna/luna.mjs";

export class Parallax_Layer
    extends PIXI.Container
{
    //--------------------------------------------------------------------------
    constructor(texture, fill_width)
    {
        super();
        this.sprites = [];

        this._init_sprites(texture, fill_width);
    } // CTOR

    //--------------------------------------------------------------------------
    move_parallax(speed, factor)
    {
        const move_speed = speed * factor
        for(let i = this.sprites.length -1; i >= 0; --i) {
            const sprite = this.sprites[i];
            sprite.x += move_speed;
            if(sprite.x + sprite.width < 0) {
                luna.Arr.pop_front(this.sprites);
                sprite.x = luna.Arr.get_back(this.sprites).x + sprite.width;
                luna.Arr.push_back(this.sprites, sprite);
            }
        }
    } // MoveParallax

    //--------------------------------------------------------------------------
    _init_sprites(texture, fill_width)
    {
        const sprite = new PIXI.Sprite(texture);
        this.addChild(sprite);
        this.sprites.push(sprite);

        let total_width = this.sprites[0].width;
        do {
            const sprite = new PIXI.Sprite(texture);

            sprite.x     = total_width;
            total_width += sprite.width;

            this.addChild(sprite);
            this.sprites.push(sprite);
        } while(total_width < fill_width);
    } // _InitializeSprites
} // ParallaxLayer
