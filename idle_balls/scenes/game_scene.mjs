import { luna }              from "../../libs/ark_luna/luna/luna.mjs";
import { Game_Settings }     from "../game_settings.mjs";
import { Game_Utils    }     from "../game_utils.mjs";
import { Nine_Slice_Button } from "../ui/nine_slice_button.mjs";

//
// Menu Scene
//
export class Game_Scene
    extends luna.Base_Scene
{
    //--------------------------------------------------------------------------
    constructor()
    {
        super();
    }
} // class MenuScene
