import { luna } from "../../libs/ark_luna/luna/luna.mjs";

const NINE_SLICE_BUTTON_PRESS_TINT  = 0xFF00FF;
const NINE_SLICE_BUTTON_HOVER_TINT  = 0xCCccCC;
const NINE_SLICE_BUTTON_NORMAL_TINT = 0xFFffFF;


function _GetTextureFromSettings(settings, key)
{
    if(key in settings) {
        return luna.RES.get_texture(settings[key]);
    }
    return null;
}

export class Nine_Slice_Button
    extends PIXI.NineSlicePlane
{
    //--------------------------------------------------------------------------
    constructor(texture_settings, slice_settings, size_settings)
    {
        const normal_texture = _GetTextureFromSettings(texture_settings, "normal");
        const hover_texture  = _GetTextureFromSettings(texture_settings, "hover" );
        const press_texture  = _GetTextureFromSettings(texture_settings, "press" );

        super(
            normal_texture,
            slice_settings.left_width,
            slice_settings.top_height,
            slice_settings.right_width,
            slice_settings.bottom_height
        );

        this.normal_texture = normal_texture;
        this.hover_texture  = hover_texture;
        this.press_texture  = press_texture;
        this.icon_sprite    = null;

        if(size_settings) {
            this.width  = size_settings.width;
            this.height = size_settings.height;
        }

        this.interactive = true; // !pixi
        this.buttonMode  = true; // !pixi

        this._setup_callbacks();
    } // CTOR

    //--------------------------------------------------------------------------
    add_icon(sprite)
    {
        luna.Layout.remove_from_parent(this.icon_sprite);

        this.icon_sprite = sprite;

        luna.Layout.add_to_parent                (this, this.icon_sprite);
        luna.Layout.set_center_relative_to_parent(this.icon_sprite);
    } // AddIcon

    //--------------------------------------------------------------------------
    on_pointer_down(callback)
    {
        this.on("pointerdown", ()=> {
            if(luna.Input_Manager.is_mouse_enabled) {
                callback();
            }
        });
    } // OnPointerDown

    //--------------------------------------------------------------------------
    _setup_callbacks()
    {
        this.mouseover = () => {
            if(this.hover_texture) {
                this.texture = this.hover_texture;
            } else {
                this._set_tint(this,      NINE_SLICE_BUTTON_HOVER_TINT);
                this._set_tint(this.icon, NINE_SLICE_BUTTON_HOVER_TINT);
            }
        }

        this.mouseout = () => {
            if(this.hover_texture) {
                this.texture = this.normal_texture;
            } else {
                this._set_tint(this,      NINE_SLICE_BUTTON_NORMAL_TINT);
                this._set_tint(this.icon, NINE_SLICE_BUTTON_NORMAL_TINT);
            }
        }

        this.mousedown = this.touchstart = () => {
            if(this.press_texture) {
                this.texture = this.press_texture;
            } else {
                this._set_tint(this,      NINE_SLICE_BUTTON_PRESS_TINT);
                this._set_tint(this.icon, NINE_SLICE_BUTTON_PRESS_TINT);
            }
        }
        this.mouseup = this.touchend = () => {
            if(this.press_texture) {
                this.texture = this.normal_texture;
            } else {
                this._set_tint(this,      NINE_SLICE_BUTTON_NORMAL_TINT);
                this._set_tint(this.icon, NINE_SLICE_BUTTON_NORMAL_TINT);
            }
        }
    } // _SetupCallbacks

    //--------------------------------------------------------------------------
    _set_tint(obj, color)
    {
        if(obj) {
            obj.tint = color;
        }
    } // _SetTint
} // NineSliceButton
